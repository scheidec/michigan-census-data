Pull MI Census Data
================

Using data from the American Community Survey from 2017, we need the
following information by zip code for the state of Michigan:

  - Median Income
  - Percent in Poverty
  - Percent Rural

### 2017 ACS Data

First, lets find the variables related to this information from ACS 5
year data from 2017. Making a data dictionary mapping original variable
names and labels will be helpful in determining if the proper variables
were pulled:

``` r
# census_api_key("INSERT_API_KEY")

# get variable names from 2015 acs data to look up relevant variables to pull
acs_2017_vars <- load_variables(2017, "acs5")

pulled_vars <- c("B19013_001", # Median income in the past 12 months (in 2017 inflation-adjusted dollars)!!Total (dollars)
                 "B17021_001", # total poverty
                 "B17021_002") # income in the past 12 months below poverty level

data_dict <- acs_2017_vars %>% 
  filter(name %in% pulled_vars)

data_dict %>% 
  kable()
```

| name        | label                                                                                          | concept                                                                            |
| :---------- | :--------------------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------- |
| B17021\_001 | Estimate\!\!Total                                                                              | POVERTY STATUS OF INDIVIDUALS IN THE PAST 12 MONTHS BY LIVING ARRANGEMENT          |
| B17021\_002 | Estimate\!\!Total\!\!Income in the past 12 months below poverty level                          | POVERTY STATUS OF INDIVIDUALS IN THE PAST 12 MONTHS BY LIVING ARRANGEMENT          |
| B19013\_001 | Estimate\!\!Median household income in the past 12 months (in 2017 inflation-adjusted dollars) | MEDIAN HOUSEHOLD INCOME IN THE PAST 12 MONTHS (IN 2017 INFLATION-ADJUSTED DOLLARS) |

We only need this for the state of Michigan, so find zipcodes from that
state only:

``` r
mi_zip <- "http://www.zipcodestogo.com/Michigan/" %>%
  read_html() %>%
  html_nodes(xpath='//*[@id="leftCol"]/table') %>%
  html_table(fill = TRUE) %>% 
  magrittr::extract2(1) %>% 
  select(zip_code = X1, city = X2, county = X3) %>% 
  slice(4:n()) %>% 
  mutate(state = "MI")

zips <- mi_zip %>% 
  pull(zip_code)
```

And then filter by those zip codes after pulling the data:

``` r
mi_acs_2017_data <- get_acs(geography = "zip code tabulation area", 
                            variables = c("B19013_001", 
                                          "B17021_001", 
                                          "B17021_002"),
                         year = 2017,
                         output = "wide") %>% 
 rename(zip_code = GEOID) %>% 
 filter(zip_code %in% zips) %>% 
 left_join(mi_zip, by = "zip_code") %>% 
 select(zip_code,
        city,
        county,
        state,
        median_income_estimate        = B19013_001E,
        median_income_moe             = B19013_001M,
        poverty_status_below_estimate = B17021_002E,
        poverty_status_below_moe      = B17021_002M,
        poverty_status_total_estimate = B17021_001E,
        poverty_status_total_moe      = B17021_001M) %>% 
  mutate(percent_poverty = poverty_status_below_estimate/poverty_status_total_estimate) %>% 
  select(zip_code, city, county, state, median_income = median_income_estimate, percent_poverty) %>% 
  arrange(zip_code)
```

    ## Getting data from the 2013-2017 5-year ACS

``` r
mi_acs_2017_data %>% 
  head() %>% 
  kable()
```

| zip\_code | city       | county      | state | median\_income | percent\_poverty |
| :-------- | :--------- | :---------- | :---- | -------------: | ---------------: |
| 48001     | Algonac    | Saint Clair | MI    |          55059 |        0.0959925 |
| 48002     | Allenton   | Saint Clair | MI    |          71375 |        0.0679491 |
| 48003     | Almont     | Lapeer      | MI    |          61671 |        0.0588620 |
| 48005     | Armada     | Macomb      | MI    |          71503 |        0.0314815 |
| 48006     | Avoca      | Saint Clair | MI    |          63750 |        0.0818228 |
| 48009     | Birmingham | Oakland     | MI    |         114537 |        0.0438896 |

To confirm these variables are what we are looking for and make sense,
let’s look at the distributions of the variables pulled:

``` r
mi_acs_2017_data %>% 
  select(-median_income) %>% 
  gather("metric", "value", 5) %>% 
  ggplot((aes(x = metric, y = value))) +
  geom_boxplot()
```

![](README_files/figure-gfm/unnamed-chunk-4-1.png)<!-- -->

``` r
mi_acs_2017_data %>% 
  gather("metric", "value", median_income) %>% 
  ggplot((aes(x = metric, y = value))) +
  geom_boxplot()
```

![](README_files/figure-gfm/unnamed-chunk-4-2.png)<!-- -->

Some potential outliers, but these can be examined later during
analysis.

Urban vs rural was last reported in the 2010 census, and it was only
reported by county. The file is downloaded from
[here](https://www.census.gov/programs-surveys/geography/guidance/geo-areas/urban-rural.html)

``` r
rural_data <- readxl::read_excel("County_Rural_Lookup.xlsx", skip = 3) %>% 
  janitor::clean_names() %>% 
  filter(state == "MI") %>% 
  select(-c(x2015_geoid, note)) %>% 
  select(county = x2015_geography_name,
         state,
         total_population = x2010_census_total_population,
         percent_rural = x2010_census_percent_rural) %>% 
  mutate(county = str_remove(county, " County, Michigan"))

rural_data %>% 
  head() %>% 
  kable()
```

| county  | state | total\_population | percent\_rural |
| :------ | :---- | ----------------: | -------------: |
| Alcona  | MI    |             10942 |       98.92159 |
| Alger   | MI    |              9601 |       69.04489 |
| Allegan | MI    |            111408 |       64.46754 |
| Alpena  | MI    |             29598 |       51.82783 |
| Antrim  | MI    |             23580 |      100.00000 |
| Arenac  | MI    |             15899 |      100.00000 |

Now save this in `.csv`
format:

``` r
write_csv(mi_acs_2017_data, "mi_median_income_percent_poverty_2017_acs.csv")
write_csv(rural_data, "mi_percent_rural_2010_census.csv")
```

Combine `mi_acs_2017_data` with census 2010 estimates of percent rural
from Jeremy.

``` r
zip_2013 <- read_csv("zip_2013.csv")
```

    ## Parsed with column specification:
    ## cols(
    ##   zip = col_double(),
    ##   med_inc = col_double(),
    ##   pct_poverty = col_double(),
    ##   pct_rural = col_double()
    ## )

``` r
combined <- mi_acs_2017_data %>% 
  left_join(zip_2013 %>% 
              mutate(zip_code = as.character(zip)) %>% 
              select(zip_code, pct_rural),
            by = "zip_code")

combined %>% 
  head() %>% 
  kable()
```

| zip\_code | city       | county      | state | median\_income | percent\_poverty | pct\_rural |
| :-------- | :--------- | :---------- | :---- | -------------: | ---------------: | ---------: |
| 48001     | Algonac    | Saint Clair | MI    |          55059 |        0.0959925 |         16 |
| 48002     | Allenton   | Saint Clair | MI    |          71375 |        0.0679491 |        100 |
| 48003     | Almont     | Lapeer      | MI    |          61671 |        0.0588620 |         52 |
| 48005     | Armada     | Macomb      | MI    |          71503 |        0.0314815 |        100 |
| 48006     | Avoca      | Saint Clair | MI    |          63750 |        0.0818228 |        100 |
| 48009     | Birmingham | Oakland     | MI    |         114537 |        0.0438896 |          0 |

``` r
write_csv(combined, "combined-mi-census-estimates.csv")
```
